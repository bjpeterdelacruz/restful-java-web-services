# RESTful Java Web Services

I am reading [RESTful Java Web Services, Second Edition](https://www.packtpub.com/application-development/restful-java-web-services-second-edition)
and, at the same time, developing a simple application to demonstrate my understanding of the material. However, I am doing some things differently:

* I am developing in the Eclipse IDE instead of NetBeans.
* To build the web application, I am using Gradle instead of Maven.
* The web application uses a SQLite database instead of an Oracle database. As a result, I will be creating my own models and database schema.

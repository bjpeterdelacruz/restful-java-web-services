package com.bpd.rest;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@Entity
public class Department {

  @Id
  @GeneratedValue
  private int id;
  private String departmentName;
  private String departmentHead;

  public int getId() {
    return id;
  }

  public void setDepartmentName(String departmentName) {
    this.departmentName = departmentName;
  }

  public String getDepartmentName() {
    return departmentName;
  }

  public void setDepartmentHead(String departmentHead) {
    this.departmentHead = departmentHead;
  }

  public String getDepartmentHead() {
    return departmentHead;
  }

}

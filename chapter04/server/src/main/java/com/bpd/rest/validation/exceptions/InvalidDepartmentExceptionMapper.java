package com.bpd.rest.validation.exceptions;

import javax.json.Json;
import javax.json.JsonObjectBuilder;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.ext.ExceptionMapper;

public class InvalidDepartmentExceptionMapper
    implements ExceptionMapper<ConstraintViolationException> {

  @SuppressWarnings("rawtypes")
  @Override
  public Response toResponse(ConstraintViolationException exception) {
    JsonObjectBuilder json = Json.createBuilderFactory(null).createObjectBuilder();
    json.add("status", Response.Status.BAD_REQUEST.getStatusCode());
    int counter = 1;
    for (ConstraintViolation violation : exception.getConstraintViolations()) {
      json.add(String.format("violation%d", counter++), violation.getMessage());
    }

    ResponseBuilder builder = Response.status(Response.Status.BAD_REQUEST)
        .entity(json.build().toString()).type(MediaType.APPLICATION_JSON);
    return builder.build();
  }

}

package com.bpd.rest.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import com.bpd.rest.Department;

public class NotEmptyValidator implements ConstraintValidator<NotEmpty, Department> {

  @Override
  public void initialize(NotEmpty constraintAnnotation) {

  }

  @Override
  public boolean isValid(Department value, ConstraintValidatorContext context) {
    return value.getDepartmentName() != null && !value.getDepartmentName().isEmpty()
        && value.getDepartmentHead() != null && !value.getDepartmentHead().isEmpty();
  }

}

package com.bpd.rest;

import javax.ws.rs.ApplicationPath;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.ServerProperties;
import com.bpd.rest.validation.exceptions.InvalidDepartmentExceptionMapper;

@ApplicationPath("api")
public class ApplicationConfig extends ResourceConfig {

  public ApplicationConfig() {
    packages("com.bpd.rest");
    property(ServerProperties.RESPONSE_SET_STATUS_OVER_SEND_ERROR, "true");
    register(InvalidDepartmentExceptionMapper.class);
  }

}

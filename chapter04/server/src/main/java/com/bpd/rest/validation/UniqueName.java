package com.bpd.rest.validation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;

@Constraint(validatedBy = {UniqueNameValidator.class})
@Target({ElementType.FIELD, ElementType.PARAMETER})
@Retention(value = RetentionPolicy.RUNTIME)
public @interface UniqueName {

  String message() default "{com.bpd.rest.validation.dept.exists}";
  Class<?>[] groups() default {};
  Class<? extends Payload>[] payload() default {};

}

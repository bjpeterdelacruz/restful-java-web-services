package com.bpd.rest.validation;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import com.bpd.rest.Department;

public class UniqueNameValidator implements ConstraintValidator<UniqueName, Department> {

  private final EntityManager entityManager;

  public UniqueNameValidator() {
    entityManager = Persistence.createEntityManagerFactory("jpa-test").createEntityManager();
  }

  @Override
  public void initialize(UniqueName constraintAnnotation) {

  }

  @Override
  public boolean isValid(Department value, ConstraintValidatorContext context) {
    return !isDepartmentExists(value.getDepartmentName());
  }

  private boolean isDepartmentExists(String departmentName) {
    Query query = entityManager.createQuery("SELECT dept.departmentName FROM Department dept");
    for (Object o : query.getResultList()) {
      if (o.toString().equalsIgnoreCase(departmentName)) {
        return true;
      }
    }
    return false;
  }

}

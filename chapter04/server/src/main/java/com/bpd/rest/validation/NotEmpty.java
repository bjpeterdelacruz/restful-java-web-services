package com.bpd.rest.validation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;

@Constraint(validatedBy = {NotEmptyValidator.class})
@Target({ElementType.FIELD, ElementType.PARAMETER})
@Retention(value = RetentionPolicy.RUNTIME)
public @interface NotEmpty {

  String message() default "{com.bpd.rest.validation.dept.empty}";
  Class<?>[] groups() default {};
  Class<? extends Payload>[] payload() default {};

}

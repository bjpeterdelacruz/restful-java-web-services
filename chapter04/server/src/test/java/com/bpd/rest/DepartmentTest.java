package com.bpd.rest;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import org.junit.Test;
import com.bpd.rest.Department;

public class DepartmentTest {

  @Test
  public void testJpa() throws Exception {
    // Setup the entity manager
    EntityManagerFactory factory = Persistence.createEntityManagerFactory("jpa-test");
    EntityManager em = factory.createEntityManager();
    // Create it
    Department dept = new Department();
    dept.setDepartmentName("Computer Science");
    dept.setDepartmentHead("Miho Nishizumi");
    // Add it
    EntityTransaction trans = em.getTransaction();
    trans.begin();
    em.persist(dept);
    trans.commit();
    // Fetch them
    TypedQuery<Department> q = em.createQuery("SELECT dept FROM Department dept", Department.class);
    List<Department> results = q.getResultList();
    for (Department thing : results) {
      System.out.println(thing.getDepartmentName() + ": " + thing.getDepartmentHead());
    }
    // Close the entity manager
    em.close();
    factory.close();
  }

}

package com.bpd.rest;

import java.util.List;
import javax.ws.rs.core.GenericType;
import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;

public class RestClient {

  private static final String BASE_URI = "http://localhost:8080/server/api/departments";

  public static void main(String... args) {
    ResteasyClient client = new ResteasyClientBuilder().build();
    ResteasyWebTarget target = client.target(BASE_URI);
    GenericType<List<Department>> genericType = new GenericType<List<Department>>() {};
    List<Department> response = target.request().get(genericType);
    for (Department dept : response) {
      System.out.print("Department name: " + dept.getDepartmentName() + "\t");
      System.out.println("Department head: " + dept.getDepartmentHead());
    }
  }

}

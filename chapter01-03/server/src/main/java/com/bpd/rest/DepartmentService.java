package com.bpd.rest;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("departments")
@Stateless
public class DepartmentService {

  private final EntityManager entityManager;

  public DepartmentService() {
    entityManager = Persistence.createEntityManagerFactory("jpa-test").createEntityManager();
  }

  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public List<Department> findAllDepartments() {
    CriteriaQuery<Department> cq = entityManager.getCriteriaBuilder().createQuery(Department.class);
    cq.select(cq.from(Department.class));
    return entityManager.createQuery(cq).getResultList();
  }

  @POST
  @Consumes(MediaType.APPLICATION_JSON)
  public void createDepartment(Department dept) {
    EntityTransaction trans = entityManager.getTransaction();
    trans.begin();
    entityManager.persist(dept);
    trans.commit();
  }

  @PUT
  @Path("{id}")
  @Consumes(MediaType.APPLICATION_JSON)
  public void editDepartment(@PathParam("id") int id, Department newDept) {
    Department oldDept = entityManager.find(Department.class, id);
    oldDept.setDepartmentName(newDept.getDepartmentName());
    oldDept.setDepartmentHead(newDept.getDepartmentHead());

    EntityTransaction trans = entityManager.getTransaction();
    trans.begin();
    entityManager.merge(oldDept);
    trans.commit();
  }

  @DELETE
  @Path("{id}")
  public void removeDepartment(@PathParam("id") int id) {
    Department dept = entityManager.find(Department.class, id);

    EntityTransaction trans = entityManager.getTransaction();
    trans.begin();
    entityManager.remove(entityManager.merge(dept));
    trans.commit();
  }

}
